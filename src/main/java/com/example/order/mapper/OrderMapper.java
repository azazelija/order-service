package com.example.order.mapper;

import com.example.order.collections.OrderCollection;
import com.example.order.model.Order;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface OrderMapper extends CollectionMapper<OrderCollection, Order> {
}
