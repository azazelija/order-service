package com.example.order.mapper;

public interface CollectionMapper<C, D> {

    C dtoToCollection(D dto);

    D collectionToDto(C collection);
}
