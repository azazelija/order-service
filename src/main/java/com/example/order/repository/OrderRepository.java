package com.example.order.repository;

import com.example.order.collections.OrderCollection;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface OrderRepository extends MongoRepository<OrderCollection, ObjectId> {
    Optional<OrderCollection> findById(String id);

    OrderCollection save(OrderCollection personCollection);

    void deleteById(String id);
}
