package com.example.order.repository;

import com.example.order.collections.OrderCollection;
import com.example.order.collections.ProductCollection;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends MongoRepository<ProductCollection, ObjectId> {
}
