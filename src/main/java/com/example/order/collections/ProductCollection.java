package com.example.order.collections;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductCollection {

    Long productId;

    Long total;
}
