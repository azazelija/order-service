package com.example.order.collections;

import lombok.*;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "order")
public class OrderCollection {
    @Id
    ObjectId id;

    List<ProductCollection> products;

    Status status;
}
