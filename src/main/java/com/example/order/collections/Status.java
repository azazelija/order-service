package com.example.order.collections;

public enum Status {
    NEW,
    CONFIRMED,
    SHIPPED,
    CANCELLED
}
