package com.example.order.rest;

import com.example.order.model.Order;
import com.example.order.service.OrderService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@AllArgsConstructor
@RestController
@RequestMapping(value = "/order", produces = MediaType.APPLICATION_JSON_VALUE)
public class OrderController {

    OrderService orderService;

    @GetMapping("/{id}")
    public ResponseEntity<Order> getById(@PathVariable String id) {
        Order order = orderService.getById(id);
        return new ResponseEntity<>(order, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Order> create(@RequestBody Order order) {
        Order newOrder = orderService.create(order);
        return new ResponseEntity<>(newOrder, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<Order> update(@RequestBody Order order) {
        Order updatedOrder = orderService.update(order);
        return new ResponseEntity<>(updatedOrder, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteById(@PathVariable String id) {
        orderService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/confirm/{id}")
    public ResponseEntity<Order> confirm(@PathVariable String id) {
        Order updatedOrder = orderService.confirm(id);
        return new ResponseEntity<>(updatedOrder, HttpStatus.OK);
    }

    @PostMapping("/cancel/{id}")
    public ResponseEntity<Order> cancel(@PathVariable String id) {
        Order updatedOrder = orderService.cancel(id);
        return new ResponseEntity<>(updatedOrder, HttpStatus.OK);
    }

    @PostMapping("/ship/{id}")
    public ResponseEntity<Order> ship(@PathVariable String id) {
        Order updatedOrder = orderService.ship(id);
        return new ResponseEntity<>(updatedOrder, HttpStatus.OK);
    }
}
