package com.example.order.io.mongock;

import com.example.order.collections.OrderCollection;
import com.example.order.collections.ProductCollection;
import com.example.order.collections.Status;
import com.example.order.repository.OrderRepository;
import com.example.order.repository.ProductRepository;
import io.mongock.api.annotations.ChangeUnit;
import io.mongock.api.annotations.Execution;
import io.mongock.api.annotations.RollbackExecution;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.ArrayList;
import java.util.List;

@ChangeUnit(id="order-initializer", order = "1", author = "mongock")
public class DatabaseInitChangeLog {

    private final MongoTemplate mongoTemplate;
    private final OrderRepository orderRepository;
    private final ProductRepository productRepository;

    public DatabaseInitChangeLog(MongoTemplate mongoTemplate,
                                 OrderRepository orderRepository,
                                 ProductRepository productRepository) {
        this.mongoTemplate = mongoTemplate;
        this.orderRepository = orderRepository;
        this.productRepository = productRepository;
    }

    /** This is the method with the migration code **/
    @Execution
    public void changeSet() {
        ProductCollection product1 = ProductCollection.builder()
                        .productId(1L)
                        .total(100L)
                        .build();
        ProductCollection product2 = ProductCollection.builder()
                        .productId(2L)
                        .total(200L)
                        .build();

        List<ProductCollection> products = new ArrayList<>();
        products.add(product1);
        products.add(product2);

        products = productRepository.saveAll(products);

        orderRepository.save(OrderCollection.builder()
                                        .products(products)
                                        .status(Status.NEW)
                                        .build());
    }

    /**
     This method is mandatory even when transactions are enabled.
     They are used in the undo operation and any other scenario where transactions are not an option.
     However, note that when transactions are avialble and Mongock need to rollback, this method is ignored.
     **/
    @RollbackExecution
    public void rollback() {

    }
}
