package com.example.order.service.impl;

import com.example.order.collections.OrderCollection;
import com.example.order.collections.ProductCollection;
import com.example.order.collections.Status;
import com.example.order.exception.EntityNotFoundException;
import com.example.order.mapper.OrderMapper;
import com.example.order.model.Order;
import com.example.order.repository.OrderRepository;
import com.example.order.service.OrderService;
import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.Optional;

@Service
public class OrderServiceImpl implements OrderService {

    private final String reduceReservedUrl = "/product/reduce/reserved/%s/%s";
    private final String increaseReservedUrl = "/product/increase/reserved/%s/%s";

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    OrderMapper orderMapper;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    DiscoveryClient discoveryClient;

    public Order getById(String id) {
        Optional<OrderCollection> orderCollection = orderRepository.findById(id);
        if (orderCollection.isPresent()) {
            return orderMapper.collectionToDto(orderCollection.get());
        }
        throw new EntityNotFoundException(EntityNotFoundException.NOT_FOUND_UNDER_ID);
    }

    public Order create(Order order) {
        OrderCollection orderCollection = orderMapper.dtoToCollection(order);
        orderCollection = orderRepository.save(orderCollection);

        order.setId(orderCollection.getId());
        return order;
    }

    public Order update(Order order) {
        Optional<OrderCollection> orderCollectionOptional = orderRepository.findById(order.getId());
        if (!orderCollectionOptional.isPresent()) {
            throw new EntityNotFoundException(EntityNotFoundException.NOT_FOUND_UNDER_ID);
        }
        OrderCollection orderCollection = orderMapper.dtoToCollection(order);
        orderRepository.save(orderCollection);

        return order;
    }

    public void deleteById(String id) {
        orderRepository.deleteById(id);
    }

    public Order confirm(String id) {
        Optional<OrderCollection> orderCollection = orderRepository.findById(id);
        if (orderCollection.isPresent()) {
            OrderCollection order = orderCollection.get();
            send(order, reduceReservedUrl);
            order.setStatus(Status.CONFIRMED);
            orderRepository.save(order);
            return orderMapper.collectionToDto(order);
        }
        throw new EntityNotFoundException(EntityNotFoundException.NOT_FOUND_UNDER_ID);
    }

    public Order cancel(String id) {
        Optional<OrderCollection> orderCollection = orderRepository.findById(id);
        if (orderCollection.isPresent()) {
            OrderCollection order = orderCollection.get();
            send(order, increaseReservedUrl);
            orderCollection.get().setStatus(Status.CANCELLED);
            orderRepository.save(order);
            return orderMapper.collectionToDto(order);
        }
        throw new EntityNotFoundException(EntityNotFoundException.NOT_FOUND_UNDER_ID);
    }

    public Order ship(String id) {
        Optional<OrderCollection> orderCollection = orderRepository.findById(id);
        if (orderCollection.isPresent()) {
            orderCollection.get().setStatus(Status.SHIPPED);
            orderRepository.save(orderCollection.get());
            return orderMapper.collectionToDto(orderCollection.get());
        }
        throw new EntityNotFoundException(EntityNotFoundException.NOT_FOUND_UNDER_ID);
    }

    private void send(OrderCollection orderCollection, String url) {
        ServiceInstance instance = discoveryClient.getInstances("product-service")
                .stream().findAny()
                .orElseThrow(() -> new IllegalStateException("product-service unavailable"));

        List<ProductCollection> productCollections = orderCollection.getProducts();
        for (ProductCollection product : productCollections) {
            UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder
                    .fromHttpUrl(instance.getUri().toString() +
                            String.format(url, product.getProductId(), product.getTotal()));

            restTemplate.postForObject(uriComponentsBuilder.toUriString(), null, Void.class);
        }
    }
}
