package com.example.order.service;

import com.example.order.model.Order;

public interface OrderService {

    Order getById(String id);

    Order create(Order order);

    Order update(Order order);

    void deleteById(String id);

    Order confirm(String id);

    Order cancel(String id);

    Order ship(String id);
}
