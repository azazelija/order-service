package com.example.order.model;

import com.example.order.collections.ProductCollection;
import com.example.order.collections.Status;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.*;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import java.util.List;
import java.util.Map;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Order {

    @JsonSerialize(using = ToStringSerializer.class)
    ObjectId id;

    List<ProductCollection> products;

    Status status;
}
